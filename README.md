# coursera-oops_in_cpp

This repository is the OOPS practice exercises from coursera - Accelerated Computer Science Fundamentals Specialization program which has 3 courses:
1. Object-Oriented Data Structures in C++ 
2. Ordered Data Structures
3. Unordered Data Structures

### Object-Oriented Data Structures in C++ 
This course teaches learners how to write a program in the C++ language, including how to set up a development environment for writing and debugging C++ code and how to implement data structures as C++ classes. It is the first course in the Accelerated CS Fundamentals specialization, and subsequent courses in this specialization will be using C++ as the language for implementing the data structures covered in class.

### Ordered Data Structures
In this course, you will learn new data structures for efficiently storing and retrieving data that is structured in an ordered sequence. Such data includes an alphabetical list of names, a family tree, a calendar of events or an inventory organized by part numbers. The specific data structures covered by this course include arrays, linked lists, queues, stacks, trees, binary trees, AVL trees, B-trees and heaps. This course also shows, through algorithm complexity analysis, how these structures enable the fastest algorithms to search and sort data.

### Unordered Data Structures
The Unordered Data Structures course covers the data structures and algorithms needed to implement hash tables, disjoint sets and graphs. These fundamental data structures are useful for unordered data. For example, a hash table provides immediate access to data indexed by an arbitrary key value, that could be a number (such as a memory address for cached memory), a URL (such as for a web cache) or a dictionary. Graphs are used to represent relationships between items, and this course covers several different data structures for representing graphs and several different algorithms for traversing graphs, including finding the shortest route from one node to another node. These graph algorithms will also depend on another concept called disjoint sets, so this course will also cover its data structure and associated algorithms.
